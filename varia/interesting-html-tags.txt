
(File temporarily kept in this repo)


<main>
The <main> tag specifies the main content of a document.
https://www.w3schools.com/tags/tag_main.asp

<article>
The <article> tag specifies independent, self-contained content.
https://www.w3schools.com/tags/tag_article.asp

https://stackoverflow.com/questions/43289280/is-it-more-correct-to-use-main-or-article-for-the-main-content-of-a-page-whi

<details>
Specify details that the user can open and close on demand
https://www.w3schools.com/tags/tag_details.asp

<figure> (and figcaption)
The <figure> tag specifies self-contained content, like illustrations, diagrams, photos, code listings, etc.
https://www.w3schools.com/tags/tag_figure.asp

<picture>
The <picture> tag gives web developers more flexibility in specifying image resources.
https://www.w3schools.com/tags/tag_picture.asp

<section>
https://www.w3schools.com/tags/tag_section.asp

<fieldset>
Group related elements in a form
https://www.w3schools.com/tags/tag_fieldset.asp

<aside>
The <aside> tag defines some content aside from the content it is placed in.
The aside content should be indirectly related to the surrounding content.
Tip: The <aside> content is often placed as a sidebar in a document.
Note: The <aside> element does not render as anything special in a browser. However, you can use CSS to style the <aside> element (see example below).
https://www.w3schools.com/tags/tag_aside.asp

<header>
The <header> element represents a container for introductory content or a set of navigational links.
A <header> element typically contains:
* one or more heading elements (<h1> - <h6>)
* logo or icon
* authorship information

