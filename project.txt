
https://pypi.org/project/website-generator/


## Core values

1. Ease of use
  * Easy to create your own themes
  * Easy to start the application
2. Ease of understanding code (priority is on ease of reading the code, performance is not an issue)


## Anteckningar (osorterade)

adding tags for blog posts without tags?

3.9: argparse BooleanOptionalAction
3.8: f-string {variable = }

website description:
**new param, or customizing theme?**

four different urls:
https://stackoverflow.com/a/21828923/2525237

Alternatives:
* https://github.com/sunainapai/makesite
* static site generators
* dynamic site


## Liknande projekt, inspiration, verktyg

https://github.com/sunainapai/makesite

https://www.w3schools.com/css/css_navbar.asp
https://www.w3schools.com/css/css_navbar_vertical.asp
https://www.w3schools.com/css/css_navbar_horizontal.asp

https://docs.python.org/3/library/html.parser.html

preview with mobile width
https://developer.mozilla.org/en-US/docs/Tools/Responsive_Design_Mode


## Att göra

documenation: exempel

tutorial or video

x flytta överdelen av main till getinitparams

x redesign so that no problem of running listdir several times
(even listdir may have a function)

Framtid/low-prio:
* documenting "nav-item", and "active-nav-item"
* documenting naming: "blog" (level 2), "page" (level 1)
* varying page title (maybe using the content dir name)
* open-graph meta tags https://neilpatel.com/blog/open-graph-meta-tags/
"<h3" or html_parser https://docs.python.org/3/library/html.parser.html
* ask for help with code review (J, diaspora, Seb)
* snippets: words in blog posts instead of characters
* document special case for readme.md -> index.html
* automated page internal navigation (using anchors) - id="" - <h2> <h3> Tech: Regex, eller str.replace
* ordering of directories 1_ 2_ 3_

replace
<h2
with
<h2 id=""

## Ideer

kind intention before writing blog post

private pages, which can only be seen using a guid

Image gallery (see nature gallery)
Perhaps adding the possibility to have "extensions" which can process files in other ways, and from other directories.
For example gallery could be an extension
Alt: Simply allowing the user to add another index file to a dir, with a .ignore (or .wgignore) inside that dir

image connected to blog post

Using http.server instead of webbrowser.open
httpd = http.server.HTTPServer(("localhost", 8000), http.server.BaseHTTPRequestHandler)
https://stackabuse.com/serving-files-with-pythons-simplehttpserver-module/
man skulle kunna ha ett test.py script som först startar en webbserver och sedan kör själva programmet


## Themes

Mini templates:
* site-template.html (this is what we have now)
* file_list-template.html (inside a content file)
* image_gallery-template.html (not certain if inside a file or not, but we probably want some way to navigate back)

template: top: navigation between pages; combined with left side: links inside page, to main headings (auto generated)

No icon per default?

testa themar:
* dark mode
* mobile
