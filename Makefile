# cd in Makefiles: https://stackoverflow.com/q/1789594/2525237
# Running dependendies in order: 1. target: | dep1 dep2 dep3 2. make dep1 (indented)
# Running one command after another: 1. build-pyinstaller: clean 2. make clean (indented)

.PHONY: clean
clean:
	rm -rf dist/ build/ website_generator.egg-info/
	cd websitegen && rm -rf dist/ build/
	rm -f website-generator.tar.gz

# .PHONY: local-server
# local-server:
#	python3 websitegen/main.py -f -t fancy
#	# python3 -m webbrowser -t 0.0.0.0:7802
#	python3 -m http.server 7804 --directory public &

.PHONY: build-pyinstaller
build-pyinstaller:
	make clean
	pip3 install -r dev-requirements.txt
	cd websitegen && pyinstaller main.py
	cd websitegen && cd dist && tar -czvf website-generator.tar.gz main/
	mv websitegen/dist/website-generator.tar.gz .

.PHONY: build-pypi
build-pypi:
	make clean
	pip3 install -r dev-requirements.txt
	python3 setup.py sdist bdist_wheel
	python3 -m twine upload dist/*
	python3 -c 'import webbrowser; webbrowser.open("https://pypi.org/project/website-generator/")'
	python3 -c 'import webbrowser; webbrowser.open("https://sunyatazero.gitlab.io/website-generator/")'
# --repository-url https://test.pypi.org/legacy/
#	python3 -c 'import webbrowser; webbrowser.open("https://test.pypi.org/project/website-generator/")'

.PHONY: build
build:
	make build-pyinstaller
	make build-pypi

.PHONY: run
run:
	cd websitegen && python3 main.py

# test:
