Markdown page

This is a standard text paragraph

<!--All the contents of this file will be converted to html-->

# Level 1 title

## Level 2 title

### Level 3 title

Horizontal ruler/divider:

***

## Features

List:
* item 1
* item 2
* item 3

Image:

![image-in-markdown](_img/image.jpg)

Table:<!--Using the "tables" extension-->

Column1 | Col2
---|---
Row1Col1|1,2
2:1|2,2

Code block:<!--Using the "fenced_code_blocks" extension-->

```
def my_func():
    print("Hello World!")
```

## Testing markdown extensions

### nl2br

Line 1
Line 2
