#!/usr/bin/env python3
import argparse  # -documentation: https://docs.python.org/3/howto/argparse.html
import collections
import configparser
import dataclasses
import enum
import os
import logging
import pathlib
import urllib.parse
import shutil
import re
import sys
import typing
from collections import ChainMap
from string import Template
# -documentation: https://docs.python.org/3/library/string.html#template-strings
import distutils.dir_util
import distutils.file_util
import webbrowser
import datetime
import http.server
import socketserver

"""

key words:
* content
* input
* output

prefixes for functions:
* generate_
* create_

standard_page (special: home_page)

blog_page
* blog_post_page
* blog_tag_page

resource dir
blog page
blog post

create_
get_
blog_snippet

create_html_from_template

"""

INDEX_FILE: str = "index.html"
RSS_FILE: str = "rss.xml"
DATE_ONLY_IN_FORMAT: str = "%Y-%m-%d"
TAGS: str = "tags"
GENERAL_SECTION = "general"
SETTINGS = "settings.ini"
EXAMPLE_INPUT_DIR: str = "example-content"
MARKDOWN_SUFFIX = (".md",)
HTML_SUFFIX = (".html",)

STYLE_FILE = "style.css"
FAVICON_FILE = "favicon.png"

INPUT_DIR = "input_dir"
THEME_DIR = "theme_dir"
OUTPUT_DIR = "output_dir"
FORCE_REMOVE_OUTPUT = "force_remove_output"
SERVE_ON_LOCAL_SYSTEM = "serve_on_local_system"
CONTENT_URL = "content_url"
WEBSITE_URL = "website_url"

CHOICE_TODAY = "today"
CHOICE_MTIME = "mtime"


@dataclasses.dataclass(frozen=True)
class InitialValues:
    """
    *All paths can be either absolute or relative* (relative to the working dir)

    Base url (content_base_url, website_base_url) examples:
    * https://example.com
    * https://sub.example.com
    * https://example.com/directory/

    Idea: ensuring that the paths are existing for some of these
    Option 1: Overriding pathlib.Path
    Option 2: using a setter property
    """
    base_input_dir_path: pathlib.Path
    theme_dir_path: pathlib.Path
    base_output_dir_path: pathlib.Path
    content_base_url: str
    website_base_url: str
    serve_on_local_system: bool


@dataclasses.dataclass()
class CurrentValues:
    input_dir_path: pathlib.Path
    output_dir_path: pathlib.Path


@dataclasses.dataclass(frozen=True)
class OutputData:
    path: pathlib.Path
    html: str


def read_html_from_file(i_path: pathlib.Path) -> str:
    r_html: str = ""
    if not i_path.name.lower().startswith(INDEX_FILE):
        r_html += get_page_title_html(i_path)
    with open(i_path) as f:
        file_contents: str = f.read()
    if i_path.suffix.lower() in MARKDOWN_SUFFIX:
        import mistletoe  # -Expat (MIT) license
        r_html = mistletoe.markdown(file_contents)
    elif i_path.suffix.lower() in HTML_SUFFIX:
        r_html = file_contents
    else:
        logging.warning(f"The file {i_path} is neither a markdown or html file")
    return r_html


def get_datetime_from_file_name(i_path: pathlib.Path) -> typing.Optional[datetime.datetime]:
    file_name: str = i_path.name
    file_name_parts: list = file_name.split("_")
    if len(file_name_parts) >= 2:
        date_text: str = file_name_parts[0]
        try:
            r_datetime = datetime.datetime.strptime(date_text, DATE_ONLY_IN_FORMAT)
            return r_datetime
        except ValueError:
            pass
    # unix_timestamp = int(os.path.getmtime(i_path))
    # dt = datetime.datetime.fromtimestamp(unix_timestamp)
    return None


def get_datetime_text_from_file_name(i_path: pathlib.Path) -> str:
    dt = get_datetime_from_file_name(i_path)
    if dt is None:
        return ""
    r_dt_text: str = dt.strftime("%d %B %Y")  # https://strftime.org/
    return r_dt_text


def is_page(i_path: pathlib.Path) -> bool:
    if i_path.suffix in HTML_SUFFIX + MARKDOWN_SUFFIX:
        return True
    return False


def is_resource_dir(i_path: pathlib.Path) -> bool:
    if i_path.is_dir() and i_path.name.startswith("_"):
        return True
    return False


def change_suffix(i_fd_name: str, i_new_suffix: str="") -> str:
    r_new_file_name: str = i_fd_name
    if "." in i_fd_name:
        r_new_file_name = i_fd_name.rpartition(".")[0]
    r_new_file_name = r_new_file_name + i_new_suffix
    return r_new_file_name


def create_html_from_file(i_init_vals: InitialValues, i_input_file_path: pathlib.Path, i_html_content: str) -> str:
    nav_html: str = create_nav_html(i_init_vals, i_input_file_path)

    # and not html_from_file.strip().lower().startswith("<h")

    template_file_path: str = i_init_vals.theme_dir_path / "template.html"
    with open(template_file_path, "r") as template_file:
        template = Template(template_file.read())
    website_title: str = get_title_text_from_file_name(i_input_file_path)

    # parsed_url=urllib.parse.urlparse(i_init_vals.content_base_url)
    level = get_level(i_init_vals, i_input_file_path)
    relative_link: str = level * "../"
    style_file_path_str = f"{relative_link}style.css"

    r_result_html: str = template.substitute(
        website_title=website_title,
        edit_url="",
        content=i_html_content,
        navigation=nav_html,
        internal_navigation="",
        url="",
        style_file=style_file_path_str
    )

    # TODO: <meta name="og:url" property="og:url" content="___________">
    return r_result_html


def get_text_intro_from_file(i_path: pathlib.Path) -> str:
    ret_snippet = read_html_from_file(i_path)
    ret_snippet = re.sub(r"<[^>]+>", "", ret_snippet)
    ret_snippet: str = ret_snippet[:100]
    return ret_snippet


def copy_theme_files(i_theme_dir_path: pathlib.Path, i_dest_path: pathlib.Path) -> None:
    style_file_path = i_theme_dir_path / STYLE_FILE
    favicon_file_path = i_theme_dir_path / FAVICON_FILE
    shutil.copy(style_file_path, i_dest_path)
    if os.path.isfile(favicon_file_path):
        shutil.copy(favicon_file_path, i_dest_path)


def get_title_text_from_file_name(i_path: pathlib.Path) -> str:
    r_title: str = change_suffix(i_path.name)
    r_title = r_title.split("[", maxsplit=1)[0]  # removing tags
    parts_list = r_title.split("_")
    if len(parts_list) >= 2:
        potential_date_text = parts_list[0]
        try:
            datetime.datetime.strptime(potential_date_text, DATE_ONLY_IN_FORMAT)
            r_title = "_".join(parts_list[1:])
        except ValueError:
            pass
    r_title = r_title.replace("_", " ").title()
    return r_title


def get_page_title_html(i_path: pathlib.Path) -> str:
    title_text: str = get_title_text_from_file_name(i_path)
    """
    ret_string = "<h2>"
    , i_parent_name: str = ""
    if i_parent_name:
        ret_string += f'<span class="context">{i_parent_name}: </span>'
    """
    ret_string = f"<h2>{title_text}</h2>"
    return ret_string


def get_output_file_name(i_input_file_name: str) -> str:
    r_output_file_name: str = change_suffix(i_input_file_name, ".html")
    return r_output_file_name


def get_level(i_init_vals: InitialValues, i_input_file_path: pathlib.Path) -> int:
    """

    :param i_init_vals:
    :param i_input_file_path:
    :return: Starting at level 0
    """
    base_output_dir_length: int = len(i_init_vals.base_output_dir_path.resolve().parts)
    difference: int = len(i_init_vals.base_input_dir_path.resolve().parts) - base_output_dir_length
    input_path_length: int = len(i_input_file_path.resolve().parts)
    r_level: int = input_path_length - base_output_dir_length - difference - 1
    return r_level


def create_nav_html(i_init_vals: InitialValues, i_input_file_path: pathlib.Path) -> str:
    """
    the page type determines what level we are at
    Navigation is only at the top level (no sub-menus)
    The only thing that differs in the navigation is the styling (showing which navigation entry is active)
    """

    ret_nav = collections.OrderedDict()
    input_list = list(i_init_vals.base_input_dir_path.iterdir())
    index_output_file_path_str = ""
    for path in input_list:
        output_file_name: str = get_output_file_name(path.name)
        # -.resolve() is used to get an absolute path. This must be done for this to work for all levels on the site
        nav_item_title: str = get_title_text_from_file_name(path)
        if is_page(path):
            if output_file_name.lower() == INDEX_FILE:
                nav_item_title = "Home"
                index_output_file_path_str = output_file_name
            ret_nav[output_file_name] = nav_item_title
        elif path.is_dir() and not is_resource_dir(path):
            ret_nav[output_file_name] = nav_item_title
            # ret_nav.move_to_end(output_file_name)
        else:
            logging.warning(f"File or dir type not covered. You may want to remove this file: {path}")
    if index_output_file_path_str:
        ret_nav.move_to_end(index_output_file_path_str, last=False)
        # -this moves the start/home page to the beginning (despite the name)

    level = get_level(i_init_vals, i_input_file_path)
    if level == 0:
        relative_link: str = ""
        active_nav_file: str = change_suffix(i_input_file_path.name, ".html")
    elif level == 1:
        relative_link: str = "../"
        active_nav_file: str = i_input_file_path.parent.name
    elif level == 2:
        relative_link: str = "../../"
        active_nav_file: str = i_input_file_path.parent.parent.name
    else:
        raise Exception("Case not covered")

    """
    if i_page_type == PageType.standard:
        relative_link: str = ""
        active_nav_file: str = change_suffix(i_input_file_path.name, ".html")
    elif i_page_type == PageType.blog_post:
        relative_link: str = "../"
        active_nav_file: str = i_input_file_path.parent.name
    elif i_page_type == PageType.blog_tag:
        relative_link: str = "../../"
        active_nav_file: str = i_input_file_path.parent.parent.name
    else:
        raise Exception("Case not covered")
    """


    nav_total_str = ""
    for _nav_output_path, _nav_title_str in ret_nav.items():
        active_item = ''
        if change_suffix(_nav_output_path) == change_suffix(active_nav_file):  # -active page for blog post
            active_item = 'id="active-nav-item"'
        nav_line_str = f'<a {active_item} class="nav-item" href="{relative_link}{_nav_output_path}">{_nav_title_str}</a>\n'
        # -adding href here so that the user can navigate back to the blog overview page
        nav_total_str += nav_line_str



    return nav_total_str


def get_tags(i_path: pathlib.Path) -> list[str]:
    regexp: str = r"\[([^]]+)"
    ret_tags = re.findall(regexp, i_path.name)
    return ret_tags


def create_output_dir(i_output_dir_path: pathlib.Path, i_force_remove_output: bool) -> None:
    """
    Takes a relative or absolute path. Creates a dir (possibly after removing it).
    """
    if i_output_dir_path.is_dir():
        remove_output_bool = False
        if not i_force_remove_output:
            user_input: str = input(f"The dir {i_output_dir_path} is not empty. "
            "Are you sure you want to remove the contents in it? (y/n)")
            remove_output_bool = user_input == "y"
        if remove_output_bool or i_force_remove_output:
            distutils.dir_util.remove_tree(i_output_dir_path)
        else:
            print("Exiting")
            sys.exit(0)
    os.mkdir(i_output_dir_path)


def generate_blog_post_page(i_init_vals: InitialValues, i_blog_dir_name: str, i_post_input_file_name: str) -> str:
    blog_post_input_file_path = i_init_vals.base_input_dir_path / i_blog_dir_name / i_post_input_file_name

    tags_for_page: list = get_tags(blog_post_input_file_path)

    tags_html_list = []
    for tag_name in tags_for_page:
        tag_file_name = tag_name + ".html"
        # rel_tag_file_path: str = os.path.join(i_init_vals.base_output_dir_path, i_blog_dir_name, TAGS, tag_file_name)
        rel_tag_file_path: str = os.path.join("..", i_blog_dir_name, TAGS, tag_file_name)
        tags_html_list.append(f'<span><a href="{rel_tag_file_path}">#{tag_name}</a></span>')
    tags_html = ", ".join(tags_html_list)

    # parent_title = get_output_file_name(i_path)
    sub_html_content_str = get_page_title_html(blog_post_input_file_path)
    sub_html_content_str += tags_html
    sub_html_content_str += read_html_from_file(blog_post_input_file_path)
    r_blog_post_html: str = create_html_from_file(i_init_vals, blog_post_input_file_path, sub_html_content_str)

    return r_blog_post_html


def generate_post_snippet_html(i_init_vals: InitialValues, i_blog_dir_name: str, i_post_input_file_name: str) -> str:
    post_input_file_path = pathlib.Path(i_init_vals.base_input_dir_path, i_blog_dir_name, i_post_input_file_name)
    post_descr: str = get_text_intro_from_file(post_input_file_path)
    post_date_text: str = get_datetime_text_from_file_name(post_input_file_path)
    post_output_file_name: str = get_output_file_name(post_input_file_path.name)
    post_title: str = get_title_text_from_file_name(post_input_file_path)
    rel_post_output_path_str: str = os.path.join(i_blog_dir_name, post_output_file_name)
    # -path is relative to the output directory
    tags_html_list = []
    for tag_name in get_tags(post_input_file_path):
        tag_file_name = tag_name + ".html"
        rel_tag_file_path: str = os.path.join(i_blog_dir_name, TAGS, tag_file_name)
        tags_html_list.append(f'<span><a href="{rel_tag_file_path}">#{tag_name}</a></span>')
    tags_html = ", ".join(tags_html_list)
    ret_snippet_html: str = f"""
    <div><h3><a href="{rel_post_output_path_str}">{post_title}</a></h3>
    {tags_html}<span> --- {post_date_text}</span>
    <p>{post_descr}</p>
    </div>
    """
    return ret_snippet_html


def is_running_on_gitlab() -> bool:
    r_running_on_gitlab_bool = False
    try:
        if os.environ["GITLAB_CI"]:
            r_running_on_gitlab_bool = True
    except KeyError:
        pass
    return r_running_on_gitlab_bool


def get_posts_for_each_tag(i_blog_input_dir_path: pathlib.Path) -> dict[str, list[str]]:
    r_tags_and_list_of_posts_dict = {}
    for input_file_path in i_blog_input_dir_path.iterdir():
        post_output_file_name = get_output_file_name(input_file_path.name)
        tags_for_page: list = get_tags(input_file_path)
        for tag in tags_for_page:
            if tag in r_tags_and_list_of_posts_dict:
                all_posts_for_tag_list = r_tags_and_list_of_posts_dict.get(tag)
            else:
                all_posts_for_tag_list = []
            all_posts_for_tag_list.append(post_output_file_name)
            r_tags_and_list_of_posts_dict[tag] = all_posts_for_tag_list
    return r_tags_and_list_of_posts_dict

    """

    r_tags_and_list_of_posts_dict = {}
    for input_file_path in i_blog_input_dir_path.iterdir():
        # content_page_file_name: str = input_file_path.name
        tags_for_content_page: list = get_tags(input_file_path)
        for tag in tags_for_content_page:
            r_tags_and_list_of_posts_dict[tag] = []

    for tag in r_tags_and_list_of_posts_dict:
        all_posts_for_tag_list = list(i_blog_input_dir_path.iterdir())
        if tag in all_posts_for_tag_list:
            
        r_tags_and_list_of_posts_dict[tag] = all_posts_for_tag_list

    return r_tags_and_list_of_posts_dict

    # A. Building tags lists
    tags_for_page: list = get_tags(post_content_file_path)
    for tag in tags_for_page:
        post_list = []
        if tag in tag_dict:
            post_list = tag_dict.get(tag)
        post_list.append(post_output_file_name)
        tag_dict[tag] = post_list
    """


def generate_tag_pages(i_init_vals: InitialValues, i_blog_dir_name: str) -> list[OutputData]:
    r_output_data = []
    blog_input_dir_path = pathlib.Path(i_init_vals.base_input_dir_path, i_blog_dir_name)
    posts_for_each_tag_dict = get_posts_for_each_tag(blog_input_dir_path)
    for tag_name, post_list in posts_for_each_tag_dict.items():
        tag_file_name: str = tag_name + ".html"
        tag_file_path: pathlib.Path = i_init_vals.base_output_dir_path / i_blog_dir_name / TAGS / tag_file_name
        inner_tag_page_html = f"<p>{tag_name}</p><ul>"
        for post_output_fn in post_list:
            inner_tag_page_html += f'<li><a href="../{post_output_fn}">{post_output_fn}</a></li>\n'
        inner_tag_page_html += "</ul>"
        # inner_tag_page_html
        # inner_tag_page_html = create_html_for_tags_overview(i_init_vals, tags_dir_path, tag_dict)
        virtual_input_tag_file_path = pathlib.Path = i_init_vals.base_input_dir_path / i_blog_dir_name / TAGS / tag_file_name
        # -"virtual" since there is not any input file, but the path is used to determine the level
        tag_page_html = create_html_from_file(i_init_vals, virtual_input_tag_file_path, inner_tag_page_html)
        r_output_data.append(OutputData(tag_file_path, tag_page_html))
    return r_output_data


def generate_blog(i_init_vals: InitialValues, i_blog_dir_name: str) -> list[OutputData]:
    """
    :param i_init_vals: Paths can be relative or absolute
    :param i_blog_dir_name: Path to the blog files
    :return:
    """
    appl_base_dir_path = pathlib.Path(__file__).parent
    r_output_list = []

    ##### output_dir_path = i_init_vals.base_output_dir_path / i_content_fd_path.name
    ##### os.mkdir(output_dir_path)  # -we need to create the new dir ourselves before we copy theme files
    ##### copy_theme_files(i_init_vals.theme_dir_path, output_dir_path)

    # Building the list of file paths (and possibly adding dates)
    blog_input_fd_path_list = []

    blog_input_dir_path = pathlib.Path(i_init_vals.base_input_dir_path, i_blog_dir_name)
    for path in blog_input_dir_path.iterdir():
        input_file_path = blog_input_dir_path / path.name

        """
        is_sub_dir_of_appl_base_path: bool = True
        if input_file_path.is_relative_to(appl_base_dir_path):
            is_sub_dir_of_appl_base_path = False
        # if ".." in os.path.relpath(input_file_path, start=appl_base_dir_path):

        running_on_gitlab: bool = is_running_on_gitlab()
        dt = get_datetime_from_file_name(input_file_path)
        if (dt is None
        and is_page(input_file_path)
        and not is_sub_dir_of_appl_base_path
        and not running_on_gitlab):
            mtime_unix_timestamp = int(os.path.getmtime(input_file_path))
            mtime_dt = datetime.datetime.fromtimestamp(mtime_unix_timestamp)
            mtime_text = mtime_dt.strftime(DATE_ONLY_IN_FORMAT)
            input_text = (f"No date found for {fd_name} - What do you choose? "
                f"today, mtime ({mtime_text}), none (default)")
            choice: str = input(input_text)
            # todays date, custom input, mtime (display), none (default)
            new_fd_path = ""
            if choice == "today":
                today_dt = datetime.datetime.today()
                today_text = today_dt.strftime(DATE_ONLY_IN_FORMAT)
                new_fd_path = str(i_content_fd_path / today_text) + "_"+fd_name
            elif choice == "mtime":
                new_fd_path = str(i_content_fd_path / mtime_text) +"_"+fd_name
            else:
                possible_fd_path = str(i_content_fd_path / choice) +"_"+fd_name
                dt = get_datetime_from_file_name(possible_fd_path)
                if dt is not None:
                    new_fd_path = possible_fd_path
            if new_fd_path:
                try:
                    os.rename(input_file_path, new_fd_path)
                    input_file_path = new_fd_path
                except:
                    pass
        """
        blog_input_fd_path_list.append(input_file_path)

    blog_base_page_html = get_page_title_html(blog_input_dir_path)
    # blog_base_page_html: str = read_html_from_file(i_input_file_path)

    rss_item_template_file_path = appl_base_dir_path / "rss_item.xml"
    with open(rss_item_template_file_path, "r") as template_file:
        rss_item_template = Template(template_file.read())
    rss_items_xml: str = ""

    feed_template_file_path = appl_base_dir_path / "rss_feed.xml"
    with open(feed_template_file_path, "r") as template_file:
        rss_feed_template = Template(template_file.read())

    # TODO: sorted(blog_input_fd_path_list, key=lambda x: get_datetime(x))

    for post_input_file_path in blog_input_fd_path_list:
        post_output_file_name: str = get_output_file_name(post_input_file_path.name)

        # B. Create post html
        post_html = generate_blog_post_page(i_init_vals, i_blog_dir_name, post_input_file_path.name)
        post_output_file_path = i_init_vals.base_output_dir_path / i_blog_dir_name / post_output_file_name
        r_output_list.append(OutputData(post_output_file_path, post_html))

        # C. Building list of blog entries on blog post_output_fn
        blog_base_page_html += generate_post_snippet_html(i_init_vals, i_blog_dir_name, post_input_file_path.name)

        # D. Creating RSS items for the post
        """
        if i_init_vals.website_base_url.endswith("/"):
            website_base_url = i_init_vals.website_base_url
        else:
            website_base_url = i_init_vals.website_base_url + "/"
        blog_post_output_file_url = website_base_url + os.path.join(blog_name, blog_post_output_file_name)
        """
        blog_post_output_file_name: str = get_output_file_name(post_input_file_path.name)
        blog_post_output_file_url: str = urllib.parse.urljoin(i_init_vals.website_base_url, i_blog_dir_name, blog_post_output_file_name)
        blog_post_title: str = get_title_text_from_file_name(post_input_file_path)
        post_descr: str = get_text_intro_from_file(post_input_file_path)
        item_string = rss_item_template.substitute(
            title=blog_post_title,
            link=str(blog_post_output_file_url),
            description=post_descr
        )
        rss_items_xml += item_string

    tags_dir_path: pathlib.Path = i_init_vals.base_output_dir_path / i_blog_dir_name / TAGS
    os.makedirs(str(tags_dir_path.resolve()))
    copy_theme_files(i_init_vals.theme_dir_path, tags_dir_path)

    # Creating a page for each tag
    # posts_for_each_tag_dict = {}  # -key: tag, value: list of posts
    tag_pages_html_dict: dict = generate_tag_pages(i_init_vals, i_blog_dir_name)
    r_output_list.extend(tag_pages_html_dict)

    # Creating an RSS feed..
    if i_init_vals.website_base_url:
        title: str = get_title_text_from_file_name(os.path.basename(i_init_vals.base_input_dir_path))
        rss_all_content_xml: str = rss_feed_template.substitute(
            title=title, link=i_init_vals.website_base_url,
            description="WEBSITE DESCR", items=rss_items_xml
        )
        rss_output_file_path_str = i_init_vals.base_output_dir_path / blog_name / RSS_FILE
        r_output_list[rss_output_file_path_str] = rss_all_content_xml
        # ..and linking to it on the blog page
        rel_rss_feed_file_path_str = os.path.join(blog_name, RSS_FILE)
        blog_base_page_html += f"<hr><a href={rel_rss_feed_file_path}>RSS feed</a>"

    output_file_name_str = get_output_file_name(blog_input_dir_path.name)
    output_file_path = i_init_vals.base_output_dir_path / output_file_name_str
    ####html_content: str = read_html_from_file(i_input_file_path)
    post_html = create_html_from_file(i_init_vals, blog_input_dir_path, blog_base_page_html)
    r_output_list.append(OutputData(output_file_path, post_html))

    return r_output_list


def generate_standard_page(i_initial_values: InitialValues, i_page_input_file_name: str) -> list[OutputData]:
    """
    Generates html for a standard page, puts this in a dict
    :return: A dictionary. The key holds the path to the output file and the contents is the html string
    """
    output_page_file_name: str = get_output_file_name(i_page_input_file_name)
    input_page_file_path: pathlib.Path = i_initial_values.base_input_dir_path / i_page_input_file_name

    output_page_file_path = i_initial_values.base_output_dir_path / output_page_file_name
    html_content: str = read_html_from_file(input_page_file_path)
    result_str = create_html_from_file(i_initial_values, input_page_file_path, html_content)

    r_output_list = [OutputData(output_page_file_path, result_str)]
    return r_output_list


def generate_output_html(i_init_vals: InitialValues) -> list[OutputData]:
    """
    High-level function for doing the central work at the base of the content dir.
    """

    copy_theme_files(i_init_vals.theme_dir_path, i_init_vals.base_output_dir_path)

    input_path_list = list(i_init_vals.base_input_dir_path.iterdir())
    r_output_list = []
    for input_path in input_path_list:
        edit_url: str = urllib.parse.urljoin(i_init_vals.content_base_url, input_path.name)
        # Finding which category the dir or file belongs to and handling each case
        if is_resource_dir(input_path):  # -for example an _img directory
            resource_output_dir_path = i_init_vals.base_output_dir_path / input_path.name
            shutil.copytree(input_path, resource_output_dir_path)
        elif input_path.is_dir():  # -blog
            blog_dir_name: str = input_path.name
            blog_output_list = generate_blog(i_init_vals, blog_dir_name)
            r_output_list.extend(blog_output_list)
        elif is_page(input_path):  # -standard page (the start page is an example)
            page_file_name: str = input_path.name
            standard_page_output_list = generate_standard_page(i_init_vals, page_file_name)
            r_output_list.extend(standard_page_output_list)
        elif input_path.name.startswith("."):  # -ex: .gitkeep
            logging.debug(f"{input_path} is a dot file (hidden) and therefore not processed")
        elif os.path.isfile(input_path):
            logging.debug(f"{input_path} is a file which is not processed since we don't cover this suffix")
        else:
            logging.warning(f"{input_path} is neither a file nor a directory")
    return r_output_list


def get_initial_values() -> InitialValues:
    """Get values from argparse and configparser (and defaults), and create initial values based on this"""
    appl_base_dir_path = pathlib.Path(__file__).parent
    logging.info(f"{appl_base_dir_path = }")
    appl_theme_dir_path = appl_base_dir_path / "themes"
    available_themes: list[str] = os.listdir(appl_theme_dir_path)

    def get_long_arg(i_cli_arg: str):
        ret_long_cli_arg: str = "--" + i_cli_arg.replace('_', '-')
        return ret_long_cli_arg

    argument_parser = argparse.ArgumentParser(
        prog="Website Generator", description="A minimalistic static website generator")
    # All dashes (-) are automatically converted to underscores by ArgumentParser
    argument_parser.add_argument(get_long_arg(INPUT_DIR), "-d",
                                 help="Base dir containing the website (input) content")
    argument_parser.add_argument(get_long_arg(THEME_DIR), "-t",
        help="Path to dir containing the theme, or the name of a built-in theme. "
        "These are the built-in themes: " + ", ".join(available_themes))
    argument_parser.add_argument(get_long_arg(OUTPUT_DIR), "-o",
        help="Path to base dir which will receive the output (html etc) which will make up the "
        "whole website")
    argument_parser.add_argument(get_long_arg(CONTENT_URL), "-c",
        help="Base URL for editing content online. Only used when your content is hosted remotely")
    argument_parser.add_argument(get_long_arg(WEBSITE_URL), "-w",
        help="Base URL for the website. Needed for generating RSS feeds")
    argument_parser.add_argument(get_long_arg(FORCE_REMOVE_OUTPUT), "-f",
        action=argparse.BooleanOptionalAction,
        help="(true/false) No prompt if and when overwriting the output dir.")
    argument_parser.add_argument(get_long_arg(SERVE_ON_LOCAL_SYSTEM), "-s",
        action=argparse.BooleanOptionalAction,
        help="(true/false) After the website has been built a local http server will be started")
    # , default=""
    tmp_args: dict = vars(argument_parser.parse_args())
    cli_args = {}
    for k, v in tmp_args.items():
        if v is not None:
            cli_args[k] = v

    config_parser = configparser.ConfigParser()
    config_parser.read(SETTINGS)
    tmp_config = {}
    try:
        tmp_config = dict(config_parser[GENERAL_SECTION])
    except KeyError:
        pass
    file_config = {}
    for k, v in tmp_config.items():
        k: str
        k = k.replace('-', '_')
        if k in (FORCE_REMOVE_OUTPUT, SERVE_ON_LOCAL_SYSTEM):
            file_config[k] = False
            fro_config_file_key: str = k.replace('_', '-')
            file_config[k] = config_parser.getboolean(GENERAL_SECTION, fro_config_file_key)
            # -raises ValueError if unclear
        else:
            file_config[k] = v

    default_params = {
        INPUT_DIR: EXAMPLE_INPUT_DIR,
        THEME_DIR: "side-nav",
        OUTPUT_DIR: "public",
        FORCE_REMOVE_OUTPUT: False,
        SERVE_ON_LOCAL_SYSTEM: False,
        CONTENT_URL: "",
        WEBSITE_URL: "",
    }

    params_chainmap = ChainMap(cli_args, file_config, default_params)
    # -inspired by one of the answers here: https://stackoverflow.com/q/48538581/2525237

    input_dir_path = pathlib.Path(params_chainmap[INPUT_DIR])
    if not input_dir_path.exists():
        input_dir_path = appl_base_dir_path / params_chainmap[INPUT_DIR]

    theme_dir_path = pathlib.Path(params_chainmap[THEME_DIR])
    if not theme_dir_path.exists():
        theme_dir_path = appl_base_dir_path / "themes" / params_chainmap[THEME_DIR]

    output_dir_path = pathlib.Path(params_chainmap[OUTPUT_DIR])
    create_output_dir(output_dir_path, params_chainmap[FORCE_REMOVE_OUTPUT])

    content_base_url_path: str = params_chainmap[CONTENT_URL]
    # content_base_url.anchor = "//"
    website_base_url_path: str = params_chainmap[WEBSITE_URL]
    # website_base_url.anchor = "//"
    serve_on_local_system: bool = params_chainmap[SERVE_ON_LOCAL_SYSTEM]

    logging.info(f"{input_dir_path = }")
    logging.info(f"{theme_dir_path = }")
    logging.info(f"{output_dir_path = }")
    logging.info(f"{content_base_url_path = }")
    logging.info(f"{website_base_url_path = }")
    logging.info(f"{serve_on_local_system = }")

    r_init_vals = InitialValues(input_dir_path, theme_dir_path, output_dir_path,
        content_base_url_path, website_base_url_path, serve_on_local_system)
    logging.info(f"{r_init_vals = }")

    return r_init_vals


def main():
    logging.basicConfig(level=logging.DEBUG)
    initial_values = get_initial_values()
    output_data_list: list[OutputData] = generate_output_html(initial_values)
    for output_data in output_data_list:
        with open(output_data.path, "w+") as output_file:
            output_file.write(output_data.html)

    if initial_values.serve_on_local_system:
        os.chdir(initial_values.base_output_dir_path)
        port = 7800
        request_handler_class = http.server.SimpleHTTPRequestHandler
        ip_address = "127.0.0.1"
        while True:
            try:
                with socketserver.TCPServer((ip_address, port), request_handler_class) as httpd:
                    webbrowser.open(f"{ip_address}:{port}")
                    logging.info(f"Starting a server at {ip_address}:{port}")
                    httpd.serve_forever()
                    break
            except OSError:  # -in case the port is not available
                port += 1


if __name__ == "__main__":
    main()
